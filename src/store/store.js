import Vue from "vue";
import Vuex from 'vuex'
import axios from "axios";
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    API_URL: 'http://localhost:8000',
    tokens:  {
      access_token: null,
      refresh_token: null,
    },
  },
  actions: {

  },
  mutations: {
    RECORD_TOKEN(state, tokens) {
      state.tokens = tokens;
      localStorage.setItem('tokens', JSON.stringify(tokens))
    },
    REFRESH_TOKEN(state, access_token) {
      state.tokens.access_token = access_token;
      let tkn = JSON.parse(localStorage.getItem('tokens'));
      tkn.access_token = access_token;
      localStorage.setItem('tokens', JSON.stringify(tkn))
    },
    EXIT_ADMIN(state, payload) {
      state.tokens.access_token = null;
      state.tokens.refresh_token = null;
      localStorage.removeItem('tokens');
    }
  },
  getters: {
    API_URL: state => state.API_URL,
    TOKEN: state => state.tokens.access_token,
    GET_REFRESH_TOKEN: state => state.tokens.refresh_token
  }
})

