import Vue from 'vue'
import Router from 'vue-router'
import AdminPage from '../view/AdminPage'
import MainPage from "../view/MainPage";
import AdminAuth from "../view/AdminAuth"
import Store from "../store/store";
import Analyzes from "../view/Analyzes";
import Directions from "../view/Directions";
import AdminStatistics from "../view/AdminStatistics";
import Settings from "../view/Settings";
Vue.use(Router)

const routes = new Router({
  mode: 'hash',
  meta: { requireAuthAdmin: false },
  routes: [
    {
      path: '/project_name',
      meta: {requireAuthAdmin: true},
      name: 'Admin',
      component: AdminPage,
      children: [
        {
          path: 'analyzes',
          meta: { requireAuthAdmin: true },
          name: 'analyzes',
          component: Analyzes,
        },
        {
          path: 'directions',
          meta: { requireAuthAdmin: true },
          name: 'directions',
          component: Directions,
        },
        {
          path: 'statistics',
          meta: { requireAuthAdmin: true },
          name: 'statistics',
          component: AdminStatistics,
        },
        {
          path: 'settings',
          meta: { requireAuthAdmin: true },
          name: 'settings',
          component: Settings,
        },
      ]
    },
    {
      path: '/',
      name: 'AdminAuth',
      component: AdminAuth
    },
  ]
})

routes.beforeEach((to, from, next) => {
  let auth_token = Store.state.tokens.access_token;
  let token_local = localStorage.getItem('tokens');
  if(to.meta.requireAuthAdmin !== undefined && to.meta.requireAuthAdmin){
    if(auth_token || token_local){
      if (to.matched.some(record => record.meta.requireAuthAdmin)) {
        next()
      }
      else {
        routes.push('/');
      }
    }else{
      routes.push('/');
    }
  }
  else{
    next();
  }
})

export default routes
