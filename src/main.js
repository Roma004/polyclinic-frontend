import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios';
import Vuex from 'vuex'
import store  from './store/store';

import 'element-ui/lib/theme-chalk/index.css'


import {Menu} from 'element-ui'
import {MenuItem} from 'element-ui'
import {MenuItemGroup} from 'element-ui'
import {Submenu} from 'element-ui'
import ToastService from 'primevue/toastservice';
import Table from 'element-ui';
import TableColumn from 'element-ui';

Vue.use(ToastService);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Submenu);
Vue.use(Table);
Vue.use(TableColumn);


Vue.use(Vuex)

import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'
locale.use(lang);
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  axios,
  store,
  components: { App },
  template: '<App/>'
})


